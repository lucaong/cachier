require 'spec_helper'

shared_examples 'Store' do
  let(:some_key) { '87awgf97toihsdac089y' }

  describe :get do
    it 'returns nil in case of a cache miss' do
      expect(store.get(some_key)).to be_nil
    end

    it 'returns the value in case of a cache hit' do
      store.set(some_key, 'xxx', {})
      expect(store.get(some_key)).to eq('xxx')
    end
  end

  describe :set do
    it 'raises an error if something else than a string is passed as a value' do
      expect { store.set(some_key, 123, {}) }.to raise_error(StandardError)
    end
  end

  describe 'get_multi and set_multi' do
    it 'get and set multiple keys and values, returning nil for misses' do
      store.set_multi [{ key: 'one', value: '1', metadata: {} }, { key: 'three', value: '3', metadata: {} }]
      expect(store.get_multi(['one', 'two', 'three'])).to eq(['1', nil, '3'])
    end
  end

  describe :process_change do
    it 'uncaches the appropriate entries based on the dependency map' do
      store.set(:foo, 'foo', { dependencies: { foos: [1, 2], bars: [3, 4] } })
      store.set(:bar, 'bar', { dependencies: { foos: [2, 3], bazs: [5] } })
      store.set(:baz, 'baz', { dependencies: { bazs: [1, 5] } })

      store.process_change(:xxx, 1)
      expect(store.get(:foo)).to eq('foo')
      expect(store.get(:bar)).to eq('bar') 
      expect(store.get(:baz)).to eq('baz') 

      store.process_change(:foos, 1)
      expect(store.get(:foo)).to be_nil
      expect(store.get(:bar)).to eq('bar') 
      expect(store.get(:baz)).to eq('baz') 

      store.process_change(:bazs, 5)
      expect(store.get(:foo)).to be_nil
      expect(store.get(:bar)).to be_nil
      expect(store.get(:baz)).to be_nil
    end
  end

  describe :delete do
    it 'uncaches the given key' do
      store.set(:foo, 'foo', {})
      store.set(:bar, 'bar', {})

      store.delete(:foo)
      expect(store.get(:foo)).to be_nil
      expect(store.get(:bar)).to eq('bar') 
    end
  end
end
