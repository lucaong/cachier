require 'spec_helper'
require 'cachier/store_examples'

describe Cachier::Store::MemStore do
  it_behaves_like 'Store' do
    let(:store) { Cachier::Store::MemStore.new }
  end
end
