require 'spec_helper'

describe Cachier::Entry do
  let(:entry) { Cachier::Entry.new([:foo, :bar]) }

  describe :key_parts do
    it 'returns the key parts' do
      expect(entry.key_parts).to eq([:foo, :bar])
    end
  end

  describe :key do
    it 'returns the key computed from key parts' do
      expect(entry.key).to eq('foo:bar')
    end
  end

  describe :has_value do
    it 'stores the entry value' do
      entry.has_value 'xyz'
      expect(entry.value).to eq('xyz')
    end
  end

  describe :depends_on do
    it 'stores the dependencies for later inspection' do
      entry.depends_on :foos, 42
      entry.depends_on :bars, 46
      entry.depends_on :bars, [105, 99]
      expect(entry.metadata[:dependencies]).to eq(foos: [42], bars: [46, 105, 99])
    end
  end

  describe :expires_in do
    it 'stores the ttl for later inspection' do
      entry.expires_in 120
      expect(entry.metadata[:ttl]).to eq(120)
    end
  end

  describe :expires_at do
    it 'stores the expiration timestamp for later inspection' do
      time = Time.now
      entry.expires_at time
      expect(entry.metadata[:expiration]).to eq(time.to_i)
    end
  end

  describe :to_h do
    it 'returns key, value and metadata as a hash' do
      time = Time.now
      entry.depends_on :foos, 42
      entry.depends_on :bars, 46
      entry.expires_at time
      entry.expires_in 120
      entry.has_value 'xxx'
      expect(entry.to_h).to eq({
        key: 'foo:bar',
        value: 'xxx',
        metadata: {
          ttl: 120,
          expiration: time.to_i,
          dependencies: { foos: [42], bars: [46] }
        }
      })
    end
  end
end
