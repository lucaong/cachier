require 'spec_helper'

describe Cachier do
  let(:store) do
    store = double('store')
  end

  let(:cache) do
    Cachier.new(store: store)
  end

  it 'has a version number' do
    expect(Cachier::VERSION).not_to be nil
  end

  describe :get do
    it 'joins the key parts into a single string key' do
      expect(store).to receive(:get).with('foo:bar:baz').and_return 'xxx'
      cache.get :foo, :bar, :baz do; end
    end

    context 'in case of a cache hit' do
      it 'returns the value without executing the block' do
        expect(store).to receive(:get).with('foo').and_return 'xyz'
        value = cache.get :foo do |_, _|
          raise 'boom!'
        end
        expect(value).to eq('xyz')
      end
    end

    context 'in case of a cache miss' do
      it 'executes the block, stores the value and the dependencies and returns it' do
        entry = Cachier::Entry.new([:foo, :bar])
        allow(entry).to receive(:metadata).and_return(foo: 'bar')
        expect(store).to receive(:get).with('foo:bar').and_return nil
        expect(store).to receive(:set).with('foo:bar', 'zyx', entry.metadata)
        expect(Cachier::Entry).to receive(:new).once.and_return entry
        value = cache.get :foo, :bar do |key_parts, d|
          expect(key_parts).to eq([:foo, :bar])
          expect(d).to be(entry)
          'zyx'
        end
        expect(value).to eq('zyx')
      end
    end
  end

  describe :get_multi do
    it 'joins the key parts computing their product' do
      expect(store).to receive(:get_multi).with(['foo:baz:1', 'foo:baz:2',
                                                 'foo:baz:3', 'bar:baz:1',
                                                 'bar:baz:2', 'bar:baz:3'])
        .and_return (['x'] * 6)
      cache.get_multi [:foo, :bar], :baz, [1, 2, 3] do; end
    end

    context 'in case of a cache hit' do
      it 'returns the value without executing the block' do
        expect(store).to receive(:get_multi).with(['foo', 'bar']).and_return ['x', 'y']
        value = cache.get_multi [:foo, :bar] do |_, _|
          raise 'boom!'
        end
        expect(value).to eq(['x', 'y'])
      end
    end

    context 'in case of a cache miss' do
      it 'executes the block, stores the values and the dependencies and returns them' do
        entries = [:foo, :bar].map { |k| Cachier::Entry.new([k]) }
        entries.each do |entry|
          allow(entry).to receive(:metadata).and_return(foo: 'bar')
        end
        expect(store).to receive(:get_multi).with(['foo', 'bar']).and_return ['x', nil]
        expect(store).to receive(:set_multi).with([{ key: 'bar', value: 'zyx', metadata: entries.last.metadata }])
        expect(Cachier::Entry).to receive(:new).twice.and_return(*entries)
        values = cache.get_multi [:foo, :bar] do |es|
          expect(es.first).to be(entries.last)
          es.first.has_value 'zyx'
        end
        expect(values).to eq(['x', 'zyx'])
      end
    end
  end

  describe :delete do
    it 'delegates to store.delete' do
      expect(store).to receive(:delete).with('foo:bar')
      cache.delete(:foo, :bar)
    end
  end
end
