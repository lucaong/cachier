c.get :listview, 123 do |lv|
  product = Product.find(123)

  lv.depends_on :products,     product.id
  lv.depends_on :shops,        product.shop.id
  lv.depends_on :translations, product.translations.map(&:id)
  product.to_json
end

# -------------------------------------------------------------

class Listviews < LoveOS::Resource
  # ...

  def index(options)
    ids = options[:ids] || []
    cache.get_multi :listview, ids do |ids, lvs|
      products = find(ids)

      products.zip(lvs).each do |product, lv|
        lv.depends_on :products, product.id
        lv.depends_on :shops,    product.shop.id
        # ...
      end

      products.map(&:to_json)
    end.map { |lv| JSON.parse(lv) }
  end
end
