require "cachier/store/mem_store"

class Cachier
  module Store
    def self.build(store)
      # Logic to choose different store implementations would go here
      store || MemStore.new
    end
  end
end
