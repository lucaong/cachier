class Cachier
  class Entry
    attr_reader :value, :key_parts

    def initialize(key_parts)
      @key_parts = key_parts
      @map       = {}
    end

    def depends_on(table, id)
      if id.is_a? Enumerable
        id.each { |i| depends_on(table, i) }
      else
        @map[table] ||= []
        @map[table] << id
      end
    end

    def expires_in(time)
      @ttl = time.to_i
    end

    def expires_at(time)
      @expiration = time.to_i
    end

    def has_value(value)
      raise "value must be a string, #{value.class} given" unless value.is_a? String
      @value = value
    end

    def metadata
      { expiration: @expiration, ttl: @ttl, dependencies: @map }
    end

    def to_h
      { key: key, value: value, metadata: metadata }
    end

    def key
      key_parts.join(':')
    end
  end
end
