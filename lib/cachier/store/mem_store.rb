class Cachier
  module Store
    class MemStore
      def initialize(_ = nil)
        @cache        = {}
        @dependencies = Hash.new do |hash, key|
          hash[key] = []
        end
      end

      def set(key, value, metadata)
        raise "value must be a string, #{value.class} given" unless value.is_a? String
        metadata = metadata.merge(expiration: [Time.now.to_i + (metadata[:ttl] || Float::INFINITY),
                                               metadata[:expiration]].compact.min)
        @cache[key] = [value, metadata]
        set_dependencies(key, metadata[:dependencies])
      end

      def get(key)
        value, meta = @cache[key]
        if meta && meta[:expiration] && meta[:expiration] < Time.now.to_i
          delete(key)
          return nil
        end
        value
      end

      def set_multi(entries)
        entries.map { |entry| set(entry[:key], entry[:value], entry[:metadata]) }
      end

      def get_multi(keys)
        keys.map { |key| get(key) }
      end

      def delete(key)
        _, meta = @cache[key]
        (meta[:dependencies] || {}).each do |table, ids|
          ids.each do |id|
            dep_key = dependency_key(table, id)
            @dependencies[dep_key] -= key
            @dependencies.delete(dep_key) if @dependencies[dep_key].empty?
          end
        end
        @cache.delete(key)
      end
      alias_method :del, :delete

      def process_change(table, id)
        dep_key = dependency_key(table, id)
        if @dependencies.has_key?(dep_key)
          @dependencies[dep_key].each do |key|
            @cache.delete(key)
          end
          @dependencies.delete(dep_key)
        end
      end

      private def set_dependencies(key, deps)
        (deps || {}).each do |table, ids|
          ids.each do |id|
            @dependencies[dependency_key(table, id)] << key
          end
        end
      end

      private def dependency_key(table, id)
        :"#{table}:#{id}"
      end
    end
  end
end
