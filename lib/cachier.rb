require "cachier/version"
require "cachier/entry"
require "cachier/store"

class Cachier
  attr_reader :store

  def initialize(store: nil)
    @store = Store.build(store)
  end

  def get(*key_parts, &block)
    entry = Entry.new(key_parts)
    val   = store.get(entry.key)
    return val unless val.nil?
    val = block.call(key_parts, entry)
    entry.has_value(val) if entry.value.nil?
    store.set(entry.key, val, entry.metadata)
    val
  end

  def get_multi(*key_segments, &block)
    key_parts = compute_parts(key_segments)
    entries   = key_parts.map { |parts| Entry.new(parts) }
    values    = store.get_multi(entries.map(&:key))
    misses    = values.zip(entries).select { |(val, _)| val.nil? }.map(&:last)
    return values if misses.empty?
    block.call(misses)
    store.set_multi(misses.map(&:to_h))
    values.zip(entries).map do |(value, entry)|
      if value.nil? then entry.value else value end
    end
  end

  def delete(*key_parts)
    store.delete(Entry.new(key_parts).key)
  end
  alias_method :del, :delete

  private def compute_parts(segments)
    first, *rest = segments.map { |part| if part.is_a? Enumerable then part.to_a else [part] end }
    first.product(*rest)
  end
end
