# Cachier

This is a proof of concept of a caching DSL with the possibility to specify
dependency and enable cache invalidation by parsing MySQL binlog events (or any
other update event stream).

## Example of DSL usage:

```ruby
# The storage implementation is pluggable with dependency-injection, so we can
# have different implementations. There are shared examples in specs to make
# sure we always fulfill the expected interface
store = Cachier::Store::MemStore.new

cache = Cachier.new(store: store)

# Say we have a Product model, and we want to cache its JSON representation:
json = cache.get :product, I18n.locale, id do |key_parts, entry|
  _, locale, id = key_parts

  # this block is executed only in case of a cache miss, and it's used to
  # produce the cache value and to specify its dependencies
  product = Product.find(id)

  # the block argument makes it possible to specify dependencies. Just say the
  # table name and id(s) that, if changed, should cause the cache entry to be
  # invalidated
  entry.depends_on :products,     id
  entry.depends_on :shops,        product.shop_id
  entry.depends_on :translations, product.transations.map(&:id)

  # TTL and expiration can also be specified
  entry.expires_in 15.minutes
  entry.expires_at product.valid_to

  # the return value of the block is used to create the cache entry
  product.to_json
end


# In case multiple products have to be fetched at once, use `get_multi`:
json = cache.get_multi :product, I18n.locale, ids do |entries|
  # Entries objects have key_parts, key, value and metadata
  ids = entries.map { |entry| entry.key_parts.last }

  # this block is executed only once for all cache misses, so they can be
  # retrieved all at once
  products = Product.find(ids)

  # entries are used to set value and metadata (dependencies and expiration) on
  # each cache entry to be created
  products.zip(entries).each do |(product, entry)|
    entry.depends_on :products,     id
    entry.depends_on :shops,        product.shop_id
    entry.depends_on :translations, product.transations.map(&:id)
    entry.expires_in 15.minutes

    # set the cache entry value
    entry.has_value product.to_json
  end
end
```

## Design decisions:

  1. Only implement caching, leave other concerns to the user (serialization,
     retrieval, business logic, etc.)
  2. Leave the storage implementation open to different implementations (Redis,
     Memcached, Cassandra, etc.)
  3. Keep data and metadata (cache value and dependencies) in the same place, as
     they need to change together
  4. Keep dependency management flat and simple (no dependency tree), possibly
     occupying more space, but making invalidation simpler
  5. Specs, specs, specs (and shared examples for abstract interface implementations)

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'cachier'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install cachier

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/cachier.


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
